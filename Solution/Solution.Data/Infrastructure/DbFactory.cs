﻿namespace Solution.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        SolutionDbContext dbContext;

        public SolutionDbContext Init()
        {
            return dbContext ?? (dbContext = new SolutionDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
