﻿using System;

namespace Solution.Data.Infrastructure
{
    public interface IDbFactory: IDisposable
    {
        SolutionDbContext Init();
    }
}
