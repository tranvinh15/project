﻿namespace Solution.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
