﻿using Solution.Data.Infrastructure;
using Solution.Model.Models;

namespace Solution.Data.Repositories
{
    public interface IErrorRepository : IRepository<Error>
    {
        //Custom methods
    }

    public class ErrorRepository : RepositoryBase<Error>, IErrorRepository
    {
        public ErrorRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}