﻿using Solution.Data.Infrastructure;
using Solution.Model.Models; 

namespace Solution.Data.Repositories
{
    public interface IMenuRepository : IRepository<Menu>
    {
        //Custom methods
    }

    public class MenuRepository : RepositoryBase<Menu>, IMenuRepository
    {
        public MenuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
