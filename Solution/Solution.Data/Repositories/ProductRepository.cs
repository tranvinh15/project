﻿using Solution.Data.Infrastructure;
using Solution.Model.Models;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Solution.Data.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<Product> GetByExactName(string name);

        IEnumerable<Product> GetProductByUserFullname(string userName, int pageIndex, int pageSize, out int totalRow);
    }

    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Product> GetByExactName(string name)
        {
            return DbContext.Products.Where(p => p.Name.Equals(name));
        }

        public IEnumerable<Product> GetProductByUserFullname(string userName, int pageIndex, int pageSize, out int totalRow)
        {
            var query = (from products in DbContext.Products
                         select products);
            //var query = (from products in DbContext.Products
            //             join users in DbContext.Users
            //             on products.CreateBy equals users.Id
            //             where users.Fullname.Contains(userName)
            //             orderby products.CreateDate descending
            //             select products);
            totalRow = query.Count();
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return query;
        }
    }
}