﻿using Microsoft.AspNet.Identity.EntityFramework;
using Solution.Model.Models;
using System.Data.Entity;

namespace Solution.Data
{
    public class SolutionDbContext : IdentityDbContext<ApplicationUser>
    {
        public SolutionDbContext() : base("SolutionConnection")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Menu> Menus { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Error> Errors { get; set; }

        public static SolutionDbContext Create()
        {
            return new SolutionDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            //base.OnModelCreating(builder);
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId });
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId);
        }
    }
}