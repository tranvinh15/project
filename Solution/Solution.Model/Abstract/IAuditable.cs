﻿using System;

namespace Solution.Model.Abstract
{
    public interface IAuditable
    {
        DateTime? CreateDate { get; set; }
        int? CreateBy { get; set; }
        DateTime? UpdateDate { get; set; }
        int? UpdateBy { get; set; }
    }
}
