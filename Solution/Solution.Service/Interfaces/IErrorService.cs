﻿using Solution.Model.Models;

namespace Solution.Service.Interfaces
{
    public interface IErrorService
    {
        Error Create(Error error);
        void SaveChange();
    }
}
