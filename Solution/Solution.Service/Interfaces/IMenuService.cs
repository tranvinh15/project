﻿using Solution.Model.Models;
using System.Collections.Generic;

namespace Solution.Service.Interfaces
{
    public interface IMenuService
    {
        void Add(Menu menu);

        void Update(Menu menu);

        void Delete(int id);

        IEnumerable<Menu> GetAll();

        IEnumerable<Menu> GetAllPaging(int page, int pageSize, out int totalRow);

        Menu GetById(int id);

        IEnumerable<Menu> GetAllByNamePaging(string name, int page, int pageSize, out int totalRow);

        void SaveChange();
    }
}
