﻿using Solution.Model.Models;
using System.Collections.Generic;

namespace Solution.Service.Interfaces
{
    public interface IProductService
    {
        Product Add(Product product);

        void Update(Product product);

        Product Delete(int id);

        IEnumerable<Product> GetAll();

        IEnumerable<Product> GetAllPaging(int page, int pageSize, out int totalRow);

        Product GetById(int id);

        IEnumerable<Product> GetAllByNamePaging(string name, int page, int pageSize, out int totalRow);

        IEnumerable<Product> GetAllProductByUserFullNamePaging(string fullname, int page, int pageSize, out int totalRow);

        void SaveChange();
    }
}