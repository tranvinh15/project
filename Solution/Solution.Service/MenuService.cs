﻿using System;
using System.Collections.Generic;
using Solution.Model.Models;
using Solution.Service.Interfaces;
using Solution.Data.Repositories;
using Solution.Data.Infrastructure;

namespace Solution.Service
{
    public class MenuService : IMenuService
    {
        IMenuRepository _menuRepository;

        IUnitOfWork _unitOfWork;

        public MenuService(IMenuRepository menuRepository, IUnitOfWork unitOfWork)
        {
            _menuRepository = menuRepository;
            _unitOfWork = unitOfWork;
        }

        public void Add(Menu menu)
        {
            _menuRepository.Add(menu);
        }
         
        public void Delete(int id)
        {
            _menuRepository.Delete(id);
        }

        public IEnumerable<Menu> GetAll()
        {
            return _menuRepository.GetAll();
        }

        public IEnumerable<Menu> GetAllByNamePaging(string name, int page, int pageSize, out int totalRow)
        {
            return _menuRepository.GetMultiPaging(p => p.Name.Contains(name), out totalRow, page, pageSize);
        }

        public IEnumerable<Menu> GetAllPaging(int page, int pageSize, out int totalRow)
        {
            return _menuRepository.GetMultiPaging(p => p.Id > 0, out totalRow, page, pageSize);
        }

        public Menu GetById(int id)
        {
            return _menuRepository.GetSingleById(id);
        }

        public void SaveChange()
        {
            _unitOfWork.Commit();
        }

        public void Update(Menu menu)
        {
            _menuRepository.Update(menu);
        }
    }
}
