﻿using System;
using System.Collections.Generic;
using Solution.Model.Models;
using Solution.Service.Interfaces;
using Solution.Data.Repositories;
using Solution.Data.Infrastructure;

namespace Solution.Service
{
    public class ProductService : IProductService
    {
        IProductRepository _productService;

        IUnitOfWork _unitOfWork;

        public ProductService(IProductRepository productRepository, IUnitOfWork unitOfWork)
        {
            _productService = productRepository;
            _unitOfWork = unitOfWork;
        }

        public Product Add(Product product)
        {
            return _productService.Add(product);
        }

        public Product Delete(int id)
        {
            return _productService.Delete(id);
        }

        public IEnumerable<Product> GetAll()
        {
            return _productService.GetAll();
        }

        public IEnumerable<Product> GetAllByNamePaging(string name, int page, int pageSize, out int totalRow)
        {
            return _productService.GetMultiPaging(p => p.Name.Contains(name), out totalRow, page, pageSize);
        } 

        public IEnumerable<Product> GetAllPaging(int page, int pageSize, out int totalRow)
        {
            return _productService.GetMultiPaging(p => p.Id > 0, out totalRow, page, pageSize);
        }

        public IEnumerable<Product> GetAllProductByUserFullNamePaging(string fullname, int page, int pageSize, out int totalRow)
        {
            return _productService.GetProductByUserFullname(fullname, page, pageSize, out totalRow);
        } 

        public Product GetById(int id)
        {
            return _productService.GetSingleById(id);
        }

        public void SaveChange()
        {
            _unitOfWork.Commit();
        }

        public void Update(Product product)
        {
            _productService.Update(product);
        }
    }
}
