﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solution.Data.Infrastructure;
using Solution.Data.Repositories;
using Solution.Model.Models;
using System;
using System.Linq;

namespace Solution.UnitTest.RepositoryTest
{
    [TestClass]
    public class ProductRepositoryTest
    {
        IDbFactory dbFactory;
        IProductRepository objProductRepository;
        IUnitOfWork unitOfWork;

        [TestInitialize]
        public void Initialize()
        {
            dbFactory = new DbFactory();
            objProductRepository = new ProductRepository(dbFactory);
            unitOfWork = new UnitOfWork(dbFactory);
        }

        [TestMethod]
        public void Product_Repository_Create()
        {
            var allProduct = objProductRepository.GetAll();
            var count = allProduct.Count();
            Product product = new Product();
            product.Name = "Product " + (count + 1);
            product.Content = "Product for testing purpose";
            product.CreateDate = DateTime.Now;
            product.CreateBy = 1;

            var result = objProductRepository.Add(product);
            unitOfWork.Commit();

            Assert.IsTrue(result.Id > 0);
        }

        [TestMethod]
        public void Product_Repository_GetAll()
        {
            var allProduct = objProductRepository.GetAll();
            Assert.IsTrue(allProduct.Count() > 0);
        }

        [TestMethod]
        public void Product_Repository_Delete()
        {
            var firstProduct = objProductRepository.GetSingleByCondition(p => p.Id > 0);
            var deleteProduct = objProductRepository.Delete(firstProduct);

            unitOfWork.Commit();

            Assert.IsTrue(firstProduct.Id > 0);
        }
    }
}
