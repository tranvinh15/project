﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Solution.Data.Infrastructure;
using Solution.Data.Repositories;
using Solution.Model.Models;
using Solution.Service;
using Solution.Service.Interfaces;
using System;
using System.Collections.Generic;

namespace Solution.UnitTest.ServiceTest
{
    [TestClass]
    public class ProductServiceTest
    {
        private Mock<IProductRepository> _mockRepository;
        private Mock<IUnitOfWork> _mockUnitOfWork;
        private IProductService _productService;

        private List<Product> _listProduct;

        private List<Product> InitListProduct()
        {
            List<Product> listProduct = new List<Product>();
            for (int i = 0; i < 10; i++)
            {
                listProduct.Add(
                    new Product()
                    {
                        Name = "Product " + i,
                        Content = "Product for testing purpose",
                        CreateDate = DateTime.Now,
                        CreateBy = 1
                    });
            }
            return listProduct;
        }

        [TestInitialize]
        public void Initialize()
        {
            _mockRepository = new Mock<IProductRepository>();
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            _productService = new ProductService(_mockRepository.Object, _mockUnitOfWork.Object);
            _listProduct = InitListProduct();
        }

        [TestMethod]
        public void Product_Service_GetAll()
        {
            _mockRepository.Setup(mock => mock.GetAll(null)).Returns(_listProduct);

            var result = _productService.GetAll() as List<Product>;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void Product_Service_Create()
        {
            Product product = new Product();
            product.Name = "Product test create";
            product.Content = "Product for testing purpose";
            product.CreateDate = DateTime.Now;
            product.CreateBy = 1;

            _mockRepository.Setup(mock => mock.Add(product)).Returns((Product p) => {
                p.Id = 1;
                return p;
            });

            var result = _productService.Add(product);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Id > 0);
        }
    }
}