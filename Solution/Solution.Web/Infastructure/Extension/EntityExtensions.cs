﻿using Solution.Model.Models;
using Solution.Web.Models;
using System;
namespace Solution.Web.Infastructure.Extension
{
    public static class EntityExtensions
    {
        public static void UpdateProduct(this Product product, ProductViewModel productViewModel)
        {
            product.Name = productViewModel.Name;
            product.Content = productViewModel.Content;
            product.Image = productViewModel.Image;

            product.UpdateDate = DateTime.Now;
            product.UpdateBy = 1;
        }
    }
}