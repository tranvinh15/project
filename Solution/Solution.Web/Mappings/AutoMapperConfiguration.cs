﻿using Solution.Web.Models;
using Solution.Model.Models;
using AutoMapper;

namespace Solution.Web.Mappings
{
    public class AutoMapperConfiguration
    {
        
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Product, ProductViewModel>();
                cfg.CreateMap<Menu, MenuViewModel>();
            });
        }
    }
}