﻿/// <reference path="/assets/admin/libs/angular/angular.js" />

(function () {
    angular.module('solution', ['solution.common','solution.product']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$qProvider'];

    function config($stateProvider, $urlRouterProvider, $qProvider) {
        $stateProvider.state('home', {
            url: "/admin",
            templateUrl: "/app/components/home/homeView.html",
            controller: "homeController"
        });
        $urlRouterProvider.otherwise('/admin');
        $qProvider.errorOnUnhandledRejections(false);
    }
})();