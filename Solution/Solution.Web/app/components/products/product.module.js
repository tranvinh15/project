﻿(function () {
    angular.module('solution.product', ['solution.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('products', {
            url: "/products",
            templateUrl: "/app/components/products/productListView.html",
            controller: "productListController"
        }).state('product_modify', {
            url: "/product_modify",
            templateUrl: "/app/components/products/productEditView.html",
            controller: "productEditController"
        });
    }
})();